package vincent.ucgroup.kotlinapp.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import retrofit2.Response
import retrofit2.Retrofit
import vincent.ucgroup.kotlinapp.R
import vincent.ucgroup.kotlinapp.controller.LoginServiceController
import vincent.ucgroup.kotlinapp.interfaces.RetrofitResponse

class AMain : AppCompatActivity(), RetrofitResponse {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_main)

        val button : Button = findViewById(R.id.bt_app) as  Button
        button.setOnClickListener {
            val loginServiceController : LoginServiceController = LoginServiceController(this@AMain)
            loginServiceController.login(this, "bruno.gachot@gmail.com", "bruno1234")
        }
    }


    override fun onComplete(message: String) {

        Toast.makeText(this@AMain, "OnComplete : $message", Toast.LENGTH_SHORT).show()
    }

    override fun onStatusResponse(response: Response<Any>) {

        response.code()
        Toast.makeText(this@AMain, "onStatusResponse  : ${response.code()}", Toast.LENGTH_SHORT).show()
    }

    override fun onFailureResponse(message: String) {
        Toast.makeText(this@AMain, "OnComplete : $message", Toast.LENGTH_SHORT).show()
    }
}
