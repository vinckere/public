package vincent.ucgroup.kotlinapp.controller

import android.content.Context
import retrofit2.Response
import rx.Observable
import vincent.ucgroup.kotlinapp.interfaces.RetrofitResponse
import vincent.ucgroup.kotlinapp.interfaces.RetrofitService
import vincent.ucgroup.kotlinapp.retrofit.ServiceGenerator

/**
 * Fichier créé par  vincent le 21/07/2017.
 */

class LoginServiceController : BaseServiceController{

    constructor(ctx: Context){
        this.context = ctx
    }

    fun login(retrofitResponse: RetrofitResponse, email: String, password: String) : Observable<Response<Any>>{

        this.retrofitResponse = retrofitResponse

        val authentService : RetrofitService = ServiceGenerator().createService(RetrofitService::class.java)
        observable = authentService.login(email, password) as Observable<Response<Any>>
        observable()

        return observable!!
    }
}
