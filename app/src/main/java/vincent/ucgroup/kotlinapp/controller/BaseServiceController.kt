package vincent.ucgroup.kotlinapp.controller

import android.content.Context
import retrofit2.Response
import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import vincent.ucgroup.kotlinapp.interfaces.RetrofitResponse

/**
 * Fichier créé par  vincent le 21/07/2017.
 */

open class BaseServiceController() {

    protected var context: Context? = null
    protected var observable: Observable<Response<Any>>? = null
    protected var subsriber: Subscriber<Response<Any>>? = null
    protected var retrofitResponse: RetrofitResponse? = null

    protected fun subscriber() {

        subsriber = object : Subscriber<Response<Any>>() {
            override fun onNext(response: Response<Any>) {
                retrofitResponse!!.onStatusResponse(response)
            }

            override fun onCompleted() {
                retrofitResponse!!.onComplete("onCompleted()")
            }

            override fun onError(e: Throwable) {
                retrofitResponse!!.onFailureResponse(e.message.toString())
            }
        }
    }

    protected fun observable() {
        subscriber()
        observable?.subscribeOn(Schedulers.io())?.observeOn(AndroidSchedulers.mainThread())?.unsubscribeOn(Schedulers.io())?.subscribe(subsriber)
    }

}
