package vincent.ucgroup.kotlinapp.retrofit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Fichier créé par  vincent le 21/07/2017.
 */
class ServiceGenerator {

    private val PRD_API_BASE_URL = "https://easytech-dev.herokuapp.com/api/technicians/"
    private val STG_API_BASE_URL = "https://easytech-dev.herokuapp.com/api/technicians/"
    private val API_BASE_URL = STG_API_BASE_URL

    var httpClient: OkHttpClient.Builder? = null
    var interceptor: Interceptor? = null

    private var builder : Retrofit.Builder = Retrofit.Builder().baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())


    fun <T>createService(serviceClass: Class<T>) : T{
        val retrofit : Retrofit = builder.client(client()).build()
        return retrofit.create(serviceClass)
    }

    fun client() : OkHttpClient{

        httpClient = OkHttpClient.Builder()
        interceptor = Interceptor { chain ->

         val original : Request = chain!!.request()
                val request : Request = original.newBuilder()
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .method(original.method(), original.body())
                        .build()

                val response : Response = chain.proceed(request)

                if(response.isSuccessful){
                }
                response
            }

        httpClient!!.addInterceptor(interceptor)
        val client = httpClient!!.build()
        return client

    }
}