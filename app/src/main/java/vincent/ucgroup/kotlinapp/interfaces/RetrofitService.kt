package vincent.ucgroup.kotlinapp.interfaces

import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import rx.Observable

/**
 * Fichier créé par  vincent le 21/07/2017.
 */
interface RetrofitService {

    @FormUrlEncoded
    @POST("login")
    fun login(@Field("email") email: String, @Field("password") password: String): Observable<Response<JsonObject>>

}