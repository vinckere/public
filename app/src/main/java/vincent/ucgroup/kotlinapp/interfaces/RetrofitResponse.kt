package vincent.ucgroup.kotlinapp.interfaces

import retrofit2.Response

/**
 * Created by davidpayel on 24/03/2017.
 */

interface RetrofitResponse {
    fun onComplete(message: String)
    fun onStatusResponse(response: Response<Any>)
    fun onFailureResponse(message: String)
}
